const mongoose = require("mongoose");
const config = require("./config");
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const TrackHistory = require("./models/TrackHistory");
const {nanoid} = require("nanoid");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once("open", async () => {
   try  {
       await db.dropCollection('albums');
       await db.dropCollection('artists');
       await db.dropCollection('tracks');
       await db.dropCollection('trackhistories');
       await db.dropCollection('users');
   } catch (e) {
       console.log("Collection were not presented, skipping drop...");
   }

   const artist = await Artist.create({
       name: "XXXTENTACION",
       description: "Some info about X",
       image: "pab7iIpE6qPUHv5WpnqIc.jpg",
   });

   const album = await Album.create({
       name: "17",
       artist: artist._id,
       year: 2017,
       image: "7VmA7ljP9yFvpwGCUZ1CO.jpg",
   });

    await Track.create({
        name: "The Explanation",
        album: album._id,
        duration: "0:50",
        number: 1,
    },{
        name: "Jocelyn Flores",
        album: album._id,
        duration: "1:59",
        number: 2,
    },{
        name: "Depression & Obsession",
        album: album._id,
        duration: "2:24",
        number: 3,
    },{
        name: "Everybody Dies in Their Nightmares",
        album: album._id,
        duration: "1:35",
        number: 4,
    },{
        name: "Revenge",
        album: album._id,
        duration: "2:00",
        number: 5,
    },{
        name: "Save Me",
        album: album._id,
        duration: "2:43",
        number: 6,
    },{
        name: "Dead Inside (Interlude)",
        album: album._id,
        duration: "1:26",
        number: 7,
    },{
        name: "Fuck Love",
        album: album._id,
        duration: "2:26",
        number: 8,
    },{
        name: "Carry On",
        album: album._id,
        duration: "2:09",
        number: 9,
    },{
        name: "Orlando",
        album: album._id,
        duration: "2:43",
        number: 10,
    },{
        name: "Ayala (Outro)",
        album: album._id,
        duration: "1:39",
        number: 11,
    },);

    const user = await User.create({
        username: "user",
        password: "1234",
        token: nanoid(),
    });

    await TrackHistory.create({
        track: "Jocelyn Flores",
        time: "2020-11-19T10:05:25.835Z",
        user: user.username,
        artist: artist.name,
    });

    db.close();
});




