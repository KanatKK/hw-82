const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Album = require("../models/Album");
const Artists = require("../models/Artist");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    let query;
    if (req.query.artist) {
        query = {artist: req.query.artist};
    }
    try {
        const albums = await Album.find(query).sort({"year": 1}).populate("artist");
        res.send(albums);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get("/:id", async (req, res) => {
    const result = await Album.findById(req.params.id).populate("artist");
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});
router.post("/", upload.single("image"), async (req, res) => {
    const albumData = req.body;
    if (req.file) {
        albumData.image = req.file.filename
    }
    const artist = await Artists.findById(req.body.artist);
    if (!artist) return res.status(400).send("Artist does not exist");
    const album = new Album(albumData);
    try {
        await album.save();
        res.send(album);
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;