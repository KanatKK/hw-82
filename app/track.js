const router = require("express").Router();
const Album = require("../models/Album");
const Track = require("../models/Track");

router.get("/", async (req, res) => {
    let query;
    if (req.query.album) {
        query = {album: req.query.album};
    }
    try {
        const tracks = await Track.find(query).sort({"number": 1}).populate("album");
        res.send(tracks);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post("/", async (req, res) => {
    const trackData = req.body;
    const album = await Album.findById(req.body.album);
    if (!album) return res.status(400).send("Album does not exist");
    const track = new Track(trackData);
    try {
        await track.save();
        res.send(track);
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;