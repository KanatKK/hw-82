const router = require("express").Router();
const TrackHistory = require("../models/TrackHistory");
const Track = require("../models/Track");
const User = require("../models/User");

router.get("/:user", async (req, res) => {
    try {
        const trackHistory = await TrackHistory.find({'user': req.params.user}).sort({"time": -1});
        res.send(trackHistory);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const token = req.get("Authentication");
    if (!token) {
        return res.status(401).send({error: "No token presented"});
    }

    const user = await User.findOne({token});
    if(!user) {
        return res.status(401).send({error: "Wrong token"});
    }

    const track = await Track.findById(req.body.track);
    if (!track) {
        return res.status(401).send({error: "Track does not exist"});
    }

    try {
        const trackHistory = new TrackHistory(req.body);
        trackHistory.generateTime();
        trackHistory.generateUser(user.username)
        trackHistory.generateArtist(req.body.artist);
        trackHistory.generateTrack(track.name);
        await trackHistory.save();
        res.send(trackHistory);
    } catch (e) {
        return res.status(401).send("Unauthorized");
    }
});

module.exports = router;